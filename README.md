# node-config

An [Archaius](https://github.com/Netflix/archaius)-inspired dynamic configuration tool for NodeJS.

# TODO
* Consul backend

# Consul
* To run consul locally:
```docker run --rm -p 8400:8400 -p 8500:8500 -p 8600:53/udp -h node1 progrium/consul -server -bootstrap -ui-dir /ui```
* tcpdump watch:
```sudo tcpdump -i docker0 -vv -l -s0 -w - port 8500 | strings```
