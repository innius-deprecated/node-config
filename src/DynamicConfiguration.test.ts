import {assert} from "chai";
import {MemoryConfigurator} from "./configurators/MemoryConfigurator";
import {newConfiguration} from "./Configuration";
import {PropertiesConfigurator} from "./configurators/PropertiesConfigurator";
import {newScopedLogger} from "@toincrease/node-logger";
import {Observable} from "@reactivex/rxjs";

describe("DynamicConfiguration", () => {
    it("should be able to be created", (done) => {
        newConfiguration(undefined)
            .getString("foo", "bar")
            .subscribe(
            (v) => assert.equal(v.value(), "bar"),
            (e) => done(e),
            () => done()
            );
    });
    it("should be able to create a configuration with configurator", (done) => {
        let m = new MemoryConfigurator();
        newConfiguration(undefined, m)
            .getString("foo", "bar")
            .subscribe(
            (v) => assert.equal(v.value(), "bar"),
            (e) => done(e),
            () => done()
            );
    });
    it("should be able to create a configuration with configurator and values", (done) => {
        let m = new MemoryConfigurator();
        m.set("foo", "xyz");
        newConfiguration(undefined, m)
            .getString("foo", "bar")
            .subscribe(
            (v) => assert.equal(v.value(), "xyz"),
            (e) => done(e),
            () => done()
            );
    });
    it("should be able to get a number", (done) => {
        let m = new MemoryConfigurator();
        m.set("foo", 123);
        newConfiguration(undefined, m)
            .getString("foo", "bar")
            .subscribe(
            (v) => assert.equal(v.value(), 123),
            (e) => done(e),
            () => done()
            );
    });
    it("should be able to get an existing mandatory value", (done) => {
        let m = new MemoryConfigurator();
        m.set("abc", "def");
        newConfiguration(undefined, m)
            .getMandatoryString("abc")
            .subscribe(
            (v) => assert.equal(v.value(), "def"),
            (e) => done(e),
            () => done()
            );
    });
    it("should not be able to get a missing mandatory value", (done) => {
        newConfiguration(undefined)
            .getMandatoryString("abc")
            .subscribe(
            (v) => assert.fail("I should not be called"),
            (e) => {
                assert.equal(e, "getMandatoryString(abc) called, but no value found");
                done();
            },
            () => assert.fail("I should not be called")
            )
    }
    );
    it("should be able to get a bool", (done) => {
        newConfiguration(undefined)
            .getBool("foo", true)
            .subscribe(
            (v) => assert.equal(v.value(), true),
            (e) => done(e),
            () => done()
            );
    });
    it("should be able to retrieve items from the cache", (done) => {
        let m = new MemoryConfigurator();
        let c = newConfiguration(undefined, m);
        c.getString("foo", "bar")
            .do((x) => m.set("foo", "xyz"))
            .zip(c.getString("foo", "bar"), (x, y) => {
                return {
                    x: x,
                    y: y
                }
            })
            .subscribe(
            (t) => {
                assert.equal(t.x.value(), "xyz");
                assert.equal(t.y.value(), "xyz");
            },
            (e) => done(e),
            () => done()
            );
    });
    it("should show updated values when the backend changes", (done) => {
        let m = new MemoryConfigurator();
        m.set("foo", "xyz");
        newConfiguration(undefined, m)
            .getString("foo", "bar")
            .subscribe(
            (v) => {
                assert.equal(v.value(), "xyz");
                m.set("foo", "abcxyz");
                setTimeout(() => {
                    assert.equal(v.value(), "abcxyz");
                    done();
                }, 25);
            },
            (e) => done(e)
            );
    });
    it("should show updated values when the backend changes and these should be watchable as well.", (done) => {
        let m = new MemoryConfigurator();
        m.set("foo", "xyz");
        let c = newConfiguration(undefined, m);
        c.getString("foo", "bar");

        c.changes()
            .subscribe((c) => {
                assert.equal(c.key, "foo");
                assert.equal(c.value, "abcxyz");
                done();
            });

        m.set("foo", "abcxyz");
    });
    it("should be able to read config settings from two sources at once", (done) => {
        let log = newScopedLogger();
        let c1 = new MemoryConfigurator();
        c1.set("baz", "xyz");
        let c2 = new PropertiesConfigurator(log, "simple.properties");
        let c = newConfiguration(log, c1, c2);
        Observable.zip(
            c.getString("foo", "xyz"),
            c.getString("baz", "quuk"),
            (x, y) => {
                return {
                    x: x,
                    y: y
                }
            })
            .subscribe(
            (t) => {
                assert.equal(t.x.value(), "bar");
                assert.equal(t.y.value(), "xyz");
            },
            (e) => done(e),
            () => {
                c.dispose();
                done();
            }
            );
    });
    it("should be able to read config settings from a non-existing config file", (done) => {
        let log = newScopedLogger();
        let c = newConfiguration(log, new PropertiesConfigurator(log, "i-do-not-exist.properties"));
        c.getString("foo", "xyz")
            .subscribe(
            (t) => {
                assert.equal(t.value(), "xyz");
            },
            (e) => done(e),
            () => {
                c.dispose();
                done();
            }
            );
    });
    it("should be able to read a mandatory config from a second source", (done) => {
        let log = newScopedLogger();
        let c1 = new MemoryConfigurator();
        c1.set("baz", "xyz");
        let c2 = new PropertiesConfigurator(log, "simple.properties");
        let c = newConfiguration(log, c1, c2);
        c.getMandatoryString("foo")
            .subscribe(
            (t) => {
                assert.equal(t.value(), "bar");
            },
            (e) => done(e),
            () => {
                c.dispose();
                done();
            }
            );
    });
    it("should be able to dispose of the configurator", (done) => {
        let log = newScopedLogger();
        let c1 = new MemoryConfigurator();
        c1.set("baz", "xyz");
        let c2 = new PropertiesConfigurator(log, "simple.properties");
        let c = newConfiguration(log, c1, c2);
        Observable.zip(
            c.getString("foo", "xyz"),
            c.getString("baz", "quuk"),
            (x, y) => {
                return {
                    x: x,
                    y: y
                }
            })
            .subscribe(
            (t) => {
                assert.equal(t.x.value(), "bar");
                assert.equal(t.y.value(), "xyz");
            },
            (e) => done(e),
            () => {
                c.dispose();
                done();
            }
            );
    });
})
    ;
