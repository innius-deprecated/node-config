import {assert} from "chai";
import {PropertiesConfigurator} from "./PropertiesConfigurator";
import {newScopedLogger} from "@toincrease/node-logger";

import {Observable} from "@reactivex/rxjs";

describe("PropertiesConfigurator", () => {
    it("should be able to deal with nonexisting files", (done) => {
        let p = new PropertiesConfigurator(newScopedLogger(), "i-do-not-exist.properties");
        p.dispose();
        done();
    });
    it("should find entries from the file", (done) => {
        let p = new PropertiesConfigurator(newScopedLogger(), "simple.properties");
        setTimeout(() => {
            p.dispose();
            Observable.zip(
                p.find("foo"),
                p.find("answer"),
                p.find("a/recursive/entry"),
                (x, y, z) => {
                    return {
                        x: x,
                        y: y,
                        z: z
                    }
                }
            ).subscribe(
                (t) => {
                    assert.equal(t.x, "bar");
                    assert.equal(t.y, 42);
                    assert.equal(t.z, 15);
                }, (e) => done(e),
                () => done()
                );
        }, 50);
    });
    it("should not emit duplicate items", (done) => {
        let p = new PropertiesConfigurator(newScopedLogger(), "simple.properties", 100);
        setTimeout(() => p.dispose(), 500);
        p.watch()
            .takeUntil(Observable.timer(500))
            .toArray()
            .subscribe(
            (e) => {
                assert.equal(e.length, 3);
            },
            (e) => done(e),
            done
            );
    });
    it("should be able to read various file types", (done) => {
        done();
    });

    it("should be able to deal with nonexisting keys", (done) => {
        let p = new PropertiesConfigurator(newScopedLogger(), "simple.properties", 1);
        setTimeout(() => {
            p.dispose();
            let val: Observable<any> = p.find("nonexisting")
            val.subscribe(
                () => assert.fail("reading a non existing key should not emit a value"),
                (e) => done(e),
                done);
        }, 10);
    });
});
