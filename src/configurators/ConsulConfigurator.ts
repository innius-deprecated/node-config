import {Configurator} from "./Configurator";
import {Change} from "../Change";

import {Logger} from "@toincrease/node-logger";
import * as consulConstructor from "consul";
import {Watch, Consul} from "consul";

import {Observable, Subject} from "@reactivex/rxjs";

export class ConsulConfigurator implements Configurator {
    private values: { [key: string]: any; };
    private changes: Subject<any>; // PublishSubject in other Rx-varieties.
    private watches: Watch[];
    private log: Logger;
    private consul: Consul;

    /**
     * Create a new ConsulConfigurator. The port is assumed to be 8500. To configure for lacalhost:
     *
     * @example new ConsulConfigurator("localhost");
     *
     * @param log The logger used by this configurator
     * @param uri The URI for consul
     */
    constructor(log: Logger, uri: string) {
        this.values = {};
        this.watches = [];
        this.log = log;
        this.consul = consulConstructor({
            host: uri,
            port: 8500,
            defaults: {
                stale: true
            }
        });

        this.changes = new Subject<any>();
    }

    name(): string {
        return "ConsulConfigurator";
    }

    find(key: string): Observable<any> {
        if (this.values[key]) {
            return Observable.of(this.values[key]);
        } else {
            this.log.debug(`Key ${key} not found in cache; trying to retrieve it from the consul source.`);
            let w = this.createWatch(key);
            w.on("change", (data, res) => {
                if (data) {
                    let change = <Change>{
                        key: key,
                        value: data.Value,
                        valuetype: "string"
                    };
                    this.changes.next(change);
                    this.values[key] = data.Value;

                }
            });

            w.on("error", (err) => {
                this.log.error(err);
            });
            this.watches.push(w);

            return this.changes
                .filter((e) => e.key === key)
                .timeout(1000)
                .catch(() => {
                    this.log.error(`Property with key ${key} is not found.`);
                    return Observable.empty();
                })
                .take(1)
                .map((x) => x.value);
        }
    }

    watch(): Observable<Change> {
        return this.changes.asObservable();
    }

    dispose(): void {
        this.changes.complete();
        this.watches.forEach((w: Watch) => {
            w.end();
        });
    }

    private createWatch(key: string): Watch {
        return this.consul.watch({
            method: this.consul.kv.get,
            options: {
                key: key
            }
        });
    }
}
