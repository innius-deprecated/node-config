import {Configurator} from "./Configurator";
import {Change} from "../Change";

import {Observable, Subject} from "@reactivex/rxjs";

export class MemoryConfigurator implements Configurator {
    private values: { [key: string]: any; };
    private subject: Subject<Change>;

    constructor() {
        this.values = {};
        this.subject = new Subject<Change>();
    }
    name(): string {
        return "Memoryconfigurator";
    }
    find(key: string): Observable<any> {
        if (this.values[key]) {
            return Observable.of(this.values[key]);
        } else {
            return Observable.empty();
        }
    }
    watch(): Observable<Change> {
        return this.subject.asObservable();
    }
    dispose(): void {
        this.subject.complete();
    }
    set(key: string, value: any): void {
        this.values[key] = value;

        let c: Change = {
            key: key,
            value: value,
            valuetype: undefined
        };

        this.subject.next(c);
    }
}
