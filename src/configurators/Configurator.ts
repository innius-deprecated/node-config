import {Change} from "../Change";

import {Observable} from "@reactivex/rxjs";

export interface Configurator {
    /**
     * Return the name of this configurator
     * @returns string The name of this configurator.
     */
    name(): string;
    /**
     * Find a key in this configurator.
     *
     * @param key The key to retrieve.
     * @returns Observable<string> An observable of the value, that might call onError and can be empty
     */
    find(key: string): Observable<any>;
    /**
     * Watch this configurator for changes.
     * @returns {Observable<Change>} An observable with changes for this configurator.
     */
    watch(): Observable<Change>;
    /**
     * Dispose of this Configurator. Will clean up any background tasks etc. 
     */
    dispose(): void;
}
