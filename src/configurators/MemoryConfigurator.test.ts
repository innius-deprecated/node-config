import {assert} from "chai";
import {MemoryConfigurator} from "./MemoryConfigurator";

describe("MemoryConfigurator", () => {
    it("should be able to set a value that didn't exist", (done) => {
        let m = new MemoryConfigurator();

        m.find("abc")
            .subscribe(
            (x) => assert.fail("I should not be called"),
            (e) => done(e),
            () => {
                m.set("abc", "xyz");
                m.find("abc").subscribe(
                    (x) => assert.equal(x, "xyz"),
                    (e) => done(e),
                    () => done()
                )
            }
            );
    });
    it("should be able to subscribe to changes in the source", (done) => {
        let m = new MemoryConfigurator();
        m.watch()
            .take(3)
            .toArray()
            .subscribe(function(r) {
                assert.equal(r[0].key, "abc");
                assert.equal(r[0].value, 123);

                assert.equal(r[1].key, "def");
                assert.equal(r[1].value, "xyz");

                assert.equal(r[2].key, "abc");
                assert.equal(r[2].value, 1234);
            }, function(e) {
                done(e);
            }, function() {
                done()
            });
        m.set("abc", 123);
        m.set("def", "xyz");
        m.set("abc", 1234);
    });
});
