import {Configurator} from "./Configurator";
import {Change} from "../Change";

import {Logger} from "@toincrease/node-logger";

var properties = require("properties");

import {Observable, ConnectableObservable, Subscription} from "@reactivex/rxjs";

const second = 1000;
// The poling interval for consul keys:
const polling_interval = 300 * second;

export class PropertiesConfigurator implements Configurator {
    private values: { [key: string]: any; };
    private shareable: Observable<Change>;
    private subscription: Subscription;
    private filename: string;
    private log: Logger;
    private hasFired: boolean;

    /**
     * Create a new PropertiesConfigurator -- a configurator to read from a properties file.
     * @param log The logger to be used by the configurator.
     * @param file A path to the properties file.
     * @param interval An optional polling interval to read the files.
     */
    constructor(log: Logger, file: string, interval?: number) {
        this.hasFired = false;
        this.values = {};
        this.log = log;
        this.filename = file;

        if (!interval) {
            interval = polling_interval;
        }

        var that = this;

        var fileContents = () => {
            return that.safeReadFile(file);
        };

        const changes: Observable<Change> = Observable
            .interval(interval)
            .startWith(-1)
            .flatMap(() => {
                return fileContents().do({
                    complete: (): void => {
                        that.hasFired = true;
                        return;
                    }
                });
            })
            .filter((c: Change) => this.values[c.key] === undefined || this.values[c.key] !== c.value)
            .catch(() => {
                that.log.error(`Configuration file ${file} cannot be read -- skipping it from now on.`);
                return Observable.never<Change>(); // return a never completing observable, it just wont emit changes.
            });

        const conn: ConnectableObservable<Change> = changes.publish();
        this.shareable = conn;

        this.shareable
            .subscribe((c) => {
                that.values[c.key] = c.value;
            });

        this.subscription = conn.connect();
    }
    name(): string {
        return "PropertiesConfigurator";
    }
    find(key: string): Observable<any> {
        if (this.hasFired) {
            return this.values[key] ? Observable.of(this.values[key]) : Observable.empty();
        } else {
            var that = this;
            return that.safeReadFile(that.filename)
                .catch(() => {
                    return Observable.empty();
                })
                .filter((x) => x.key === key)
                .map((x) => x.value)
                .timeout(second)
                .do((x) => that.values[key] = x);
        }
    }
    watch(): Observable<Change> {
        return this.shareable;
    }
    dispose(): void {
        this.subscription.unsubscribe();
    }
    private safeReadFile(file: string): Observable<Change> {
        var that = this;
        return Observable.create((observer) => {
            that.log.debug(`Filename: ${file}`);
            properties.parse(that.filename, { path: true }, (error: any, obj: any) => {
                if (error) {
                    observer.error(error);
                } else {
                    for (var property in obj) {
                        if (obj.hasOwnProperty(property)) {
                            observer.next(<Change>{
                                key: property,
                                value: obj[property],
                                valuetype: typeof obj[property]
                            });
                        }
                    }
                    observer.complete();
                }
            });
        });
    };
}
