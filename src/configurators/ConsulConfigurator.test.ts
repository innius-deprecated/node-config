import {assert} from "chai";
import {ConsulConfigurator} from "./ConsulConfigurator";
import {Configurator} from "../configurators/Configurator";
import {newScopedLogger} from "@toincrease/node-logger";
import {Observable} from "@reactivex/rxjs";

describe("ConsulConfigurator", () => {
    let log = newScopedLogger();
    log.setLevel("error");

    // These tests are disable by default, since they require a running consul instance.
    // Precondition: have a /abc key with value 'def'.
    // docker run -it --rm --name=dev-consul -p 8500:8500 consul agent -dev -bind 0.0.0.0 -client 0.0.0.0 -ui
    // curl -XPUT -d 'def' http://localhost:8500/v1/kv/abc
    return;

    let consul: Configurator = new ConsulConfigurator(log, "localhost");

    it("should be able to get an existing value", (done) => {
        consul.find("abc")
            .first()
            .subscribe(
            (v) => assert.equal(v, "def"),
            (e) => done(e),
            done
            );
    });
    it("should not be able to get_or_create a new value", (done) => {
        consul.find("xyz")
            .subscribe(
            (v) => assert.fail("I should not be called"),
            (e) => done(e),
            done
            );
    });
    it("should be disposed", () => {
        consul.dispose();
    });
    it("should be able to watch -- will never end.", () => {
        let x: Configurator = new ConsulConfigurator(newScopedLogger(), "localhost");
        Observable.zip(
            x.find("eternal"),
            x.find("other/eternal"),
            (x, y) => {
                return {
                    x: x,
                    y: y,
                }
            }).subscribe(undefined, undefined, undefined);
        setTimeout(() => console.log("Remember, this test won't ever complete. Press C-c to abort."), 5000);
        x.watch().subscribe(console.log);
    })
});
