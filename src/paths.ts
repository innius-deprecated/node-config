/**
 * Smoothen a path (Add initial slash, remove trailing & double slashes, etc.)
 * 
 * @param path The original path
 * @returns {string} The 'smoothened' path.
 */
export function smoothen(path: string): string {
    if (path.startsWith("/")) {
        path = smoothen(path.substr(1));
    }
    if (path.endsWith("/")) {
        path = smoothen(path.substr(0, path.length - 1));
    }
    return path.replace(/\/\//g, "/");
}
