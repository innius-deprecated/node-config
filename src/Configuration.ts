import {DynamicValue} from "./DynamicValue";
import {DynamicConfiguration} from "./DynamicConfiguration";
import {Change} from "./Change";

import {Configurator} from "./configurators/Configurator";

import {Logger, newScopedLogger} from "@toincrease/node-logger";
import {Observable} from "@reactivex/rxjs";

export interface Configuration {
    /**
     * Retrieve a dynamic value representing a number.
     * @param key The key of this dynamic value.
     * @param def The default initial value of this dynamic value.
     */
    getNumber(key: string, def: number): Observable<DynamicValue<number>>;
    /**
     * Retrieve a dynamic value representing a string.
     * Return an error when a value cannot be found.
     * @param key The key of this dynamic value.
     */
    getMandatoryNumber(key: string): Observable<DynamicValue<number>>;
    /**
     * Retrieve a dynamic value representing a string.
     * @param key The key of this dynamic value.
     * @param def The default initial value of this dynamic value.
     */
    getString(key: string, def: string): Observable<DynamicValue<string>>;
    /**
     * Retrieve a dynamic value representing a string.
     * Return an error when a value cannot be found.
     * @param key The key of this dynamic value.
     */
    getMandatoryString(key: string): Observable<DynamicValue<string>>;
    /**
     * Retrieve a dynamic value representing a bool.
     * @param key The key of this dynamic value.
     * @param def The default initial value of this dynamic value.
     */
    getBool(key: string, def: boolean): Observable<DynamicValue<boolean>>;
    /**
     * Retrieve a dynamic value representing a bool.
     * Return an error when a value cannot be found.
     * @param key The key of this dynamic value.
     */
    getMandatoryBool(key: string): Observable<DynamicValue<boolean>>;
    /**
     * Observe this configuration for changes to dynamic values.
     * @returns {Observable<Change>} An observable emitting changes. 
     */
    changes(): Observable<Change>;
    /**
     * Dispose of this Configuration container.
     */
    dispose(): void;
}

export function newConfiguration(log: Logger, ...configurators: Configurator[]): Configuration {
    if (!log) {
        log = newScopedLogger();
    }

    return new DynamicConfiguration(log, ...configurators);
}
