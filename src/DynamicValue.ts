import {Logger} from "@toincrease/node-logger";

import {Observable, Subscription, ConnectableObservable, Observer} from "@reactivex/rxjs";

export interface DynamicValue<T> {
    /**
     * Retrieve the key of this dynamic value.
     * @returns string The key of this value.
     */
    key(): string;
    /**
     * Return the current value of this dynamic value. It's just a snapshot of the value at time T and wont change since
     * it is just a primitive. Calling the observable() method is advised.
     * @returns T The current value. This can change at any moment.
     */
    value(): T;
    /**
     * Return a string representation of the current value. It's just a snapshot of the value at time T and wont change since
     * it is just a primitive. Calling the observable() method is advised.
     * @returns string The current value, represented as a string.
     */
    string(): string;
    /**
     * Return an Observable of this value, representing possible changes over time.
     * @returns Observable<T> Observable with values over time.
     */
    observable(): Observable<T>;
    /**
     * Dispose this Dynamic Value, unsubscribing any subscribers whom called observable().
     */
    dispose(): void;
}

export class DynVal<T> implements DynamicValue<T> {
    private _key: string;
    private val: T;
    private obs: ConnectableObservable<T>;
    private observer: Observer<T>;
    private subcription: Subscription;
    private log: Logger;

    constructor(log: Logger, key: string, value: T) {
        this._key = key;
        this.val = value;
        this.log = log.withFields({
            "dynamicvalue": true,
            "key": key
        });

        var that = this;

        this.obs = Observable.create((o: Observer<T>) => {
            that.observer = o;
            that.propagate();
        }).publishReplay(1);
        this.subcription = this.obs.connect();
    };

    update(value: T): void {
        this.log.debug(`Updating dynamic value; was '${this.val}'; becomes '${value}'`);
        this.val = value;
        this.propagate();
    }

    key(): string {
        return this._key;
    }

    value(): T {
        return this.val;
    }

    string(): string {
        return "" + this.val;
    }

    observable(): Observable<T> {
        return this.obs;
    };

    dispose(): void {
        this.observer.complete();
        this.subcription.unsubscribe();
    }

    private propagate(): void {
        if (this.observer) {
            this.observer.next(this.val);
        }
    };

}
