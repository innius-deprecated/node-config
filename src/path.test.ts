import {assert} from "chai";
import {smoothen as s} from "./paths";

describe("path#smooth", () => {
    it("should strip preceeding slash", () => {
        assert.equal(s("/foo/bar/baz"), "foo/bar/baz");
    });
    it("should stay equal", () => {
        assert.equal(s("foo"), "foo");
    });
    it("should remove a trailing slash", () => {
        assert.equal(s("/foo/bar/"), "foo/bar");
    });
    it("should remove inner slashes", () => {
        assert.equal(s("/foo//bar///baz"), "foo/bar/baz");
    });
});
