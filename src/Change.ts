export interface Change {
    key: string;
    value: any;
    valuetype: string;
}
