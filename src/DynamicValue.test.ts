import {assert} from "chai";
import {DynVal} from "./DynamicValue";
import {newScopedLogger} from "@toincrease/node-logger";

describe("DynamicValue", () => {
    let log = newScopedLogger();
    describe("#update", () => {
        it("should be able to set a value", () => {
            let val = new DynVal<string>(log, "abc", "def");
            assert.equal(val.value(), "def");
            val.update("xyz");
            assert.equal(val.value(), "xyz");
        });
        it("should be able to set a value and read it as a string", () => {
            let val = new DynVal<number>(log, "abc", 42);
            assert.equal(val.value(), 42);
            val.update(13);
            assert.equal(val.string(), "13");
        });
        it("should be able to observe a changing value", (done) => {
            let val = new DynVal<string>(log, "abc", "def");
            var cnt = 0;
            assert.equal(val.value(), "def");
            val.observable().subscribe((v: string) => {
                cnt++;
                if (cnt === 1) {
                    assert.equal("def", v);
                } else {
                    assert.equal("xyz", v);
                    assert.equal(2, cnt);
                    done();
                }
            });
            val.update("xyz");
        });
        it("should be able to observe a changing value as a continuous stream", (done) => {
            let val = new DynVal<number>(log, "abc", 1);
            const expected = [1, 2, 3, 4, 5];

            val.observable()
                .take(5)
                .toArray()
                .subscribe(
                (x) => assert.deepEqual(expected, x),
                (e) => done(e),
                done
                );
            val.update(2);
            val.update(3);
            val.update(4);
            val.update(5);
        });
        it("should be able dispose a dynamic value.", (done) => {
            let val = new DynVal<number>(log, "abc", 1);
            const expected = [1, 2];

            val.observable()
                .take(3)
                .toArray()
                .subscribe(
                (x) => assert.deepEqual(expected, x),
                (e) => done(e),
                done
                );
            val.update(2);
            val.dispose();
            val.update(3);
        });
    });
});
