import {Configurator} from "./configurators/Configurator";
import {DynamicValue, DynVal} from "./DynamicValue";
import {Configuration} from "./Configuration";
import {Change} from "./Change";
import {smoothen} from "./paths";

import {Logger} from "@toincrease/node-logger";

import {Observable} from "@reactivex/rxjs";
import {switchIfEmpty} from "@toincrease/node-rxjs-operators";

export class DynamicConfiguration implements Configuration {
    private log: Logger;
    private configurators: Configurator[];
    private cache: { [key: string]: DynVal<any>; };
    private _changes: Observable<Change>;

    constructor(log: Logger, ...configurators: Configurator[]) {
        this.log = log;
        this.configurators = configurators;
        this.cache = {};

        this._changes = Observable
            .from<Configurator>(this.configurators)
            .flatMap((c) => c.watch());

        let that = this;

        this._changes
            .subscribe((c: Change) => {
                let cached = that.getFromCache(c.key);
                if (cached) {
                    cached.update(c.value);
                }
            });
    };

    private number(key: string): Observable<DynamicValue<number>> {
        key = smoothen(key);
        let cached = this.getFromCache(key);
        if (cached) {
            this.log.debug(`Retrieving ${key} from cache.`);
            return Observable.of(<DynamicValue<number>>cached);
        }

        return this.findInConfigurators(key, this.asNumber());
    }

    getNumber(key: string, def: number): Observable<DynamicValue<number>> {
        //noinspection TypeScriptValidateTypes
        return this.number(key)
            .let(switchIfEmpty(Observable.of(new DynVal<number>(this.log, key, def))))
            .do((dv) => this.addToCache(key, dv));
    };

    getMandatoryNumber(key: string): Observable<DynamicValue<number>> {
        //noinspection TypeScriptValidateTypes
        return this.number(key)
            .let(switchIfEmpty(Observable.throw(`getMandatoryNumber(${key}) called, but no value found`)))
            .do((dv) => this.addToCache(key, dv));
    }

    private string(key: string): Observable<DynamicValue<string>> {
        key = smoothen(key);
        let cached = this.getFromCache(key);
        if (cached) {
            this.log.debug(`Retrieving ${key} from cache.`);
            return Observable.of(<DynamicValue<string>>cached);
        }

        return this.findInConfigurators(key, this.asString());
    }

    getString(key: string, def: string): Observable<DynamicValue<string>> {
        //noinspection TypeScriptValidateTypes
        return this.string(key, def)
            .let(switchIfEmpty(Observable.of(new DynVal<string>(this.log, key, def))))
            .do((dv) => this.addToCache(key, dv));
    };

    getMandatoryString(key: string): Observable<DynamicValue<string>> {
        //noinspection TypeScriptValidateTypes
        return this.string(key)
            .let(switchIfEmpty(Observable.throw(`getMandatoryString(${key}) called, but no value found`)))
            .do((dv) => this.addToCache(key, dv));
    }

    private bool(key: string): Observable<DynamicValue<boolean>> {
        key = smoothen(key);
        let cached = this.getFromCache(key);
        if (cached) {
            this.log.debug(`Retrieving ${key} from cache.`);
            return Observable.of(<DynamicValue<boolean>>cached);
        }

        return this.findInConfigurators(key, this.asBool());
    }

    getBool(key: string, def: boolean): Observable<DynamicValue<boolean>> {
        //noinspection TypeScriptValidateTypes
        return this.bool(key)
            .let(switchIfEmpty(Observable.of(new DynVal<boolean>(this.log, key, def))))
            .do((dv) => this.addToCache(key, dv));
    };

    getMandatoryBool(key: string): Observable<DynamicValue<boolean>> {
        //noinspection TypeScriptValidateTypes
        return this.bool(key)
            .let(switchIfEmpty(Observable.throw(`getMandatoryBool(${key}) called, but no value found`)))
            .do((dv) => this.addToCache(key, dv));
    }

    changes(): Observable<Change> {
        return this._changes.share();
    }

    dispose(): void {
        this.configurators.forEach((c: Configurator) => c.dispose());
    }

    private findInConfigurators(key: string, conv: (raw: any) => any): Observable<DynVal<any>> {
        return Observable.from(this.configurators)
            .flatMap((c) => c.find(key))
            .take(1)
            .do((v) => this.log.debug(`Found value ${v} for key ${key}.`))
            .map((v) => new DynVal<any>(this.log, key, conv(v)));
    }

    private getFromCache(key: string): DynVal<any> {
        return this.cache[key];
    }

    private addToCache(key: string, d: DynVal<any>): void {
        this.cache[key] = d;
    };

    private asNumber(): (raw: any) => any {
        return (raw: any) => {
            let r = Number(raw);
            if (isNaN(r)) {
                throw `${raw} is not a valid number`;
            } else {
                return r;
            }
        };
    }

    private asString(): (a: any) => string {
        return (raw: any) => "" + raw;
    }

    private asBool(): (raw: any) => any {
        return (raw: any) => {
            raw = "" + raw; // cast any to string, so that toLowerCase() always works.
            switch (raw.toLowerCase().trim()) {
                case "true":
                case "yes":
                case "1":
                    return true;
                case "false":
                case "no":
                case "0":
                case null:
                    return false;
                default:
                    throw `${raw} is not a valid boolean`;
            }
        };
    }
}
